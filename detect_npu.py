import argparse
import os
import sys
from pathlib import Path
import time
import cv2
import numpy as np
import torch
import torch_npu  # 引入NPU支持
import torch.backends.cudnn as cudnn
import io
import contextlib

# YOLOv5相关导入
from models.experimental import attempt_load
from utils.datasets import LoadImages, LoadStreams
from utils.general import check_img_size, non_max_suppression, scale_coords, set_logging, xyxy2xywh
from utils.plots import Annotator, colors
from utils.torch_utils import select_device, time_sync

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # YOLOv5根目录
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # 添加ROOT到系统路径
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # 使路径相对化


@contextlib.contextmanager
def suppress_stderr():
    with open(os.devnull, 'w') as fnull:
        with contextlib.redirect_stderr(fnull):
            yield

@torch.no_grad()
def detect(weights=ROOT / 'yolov5s.pt',  # 模型路径
           source=ROOT / 'data/images',  # 输入源：文件/目录/URL/网络流
           imgsz=640,  # 推理图像尺寸
           conf_thres=0.25,  # 置信度阈值
           iou_thres=0.45,  # NMS IOU阈值
           max_det=1000,  # 每张图像最大检测数
           device='',  # 设备选择，空代表自动选择
           view_img=False,  # 是否显示结果
           save_txt=False,  # 是否保存结果到.txt文件
           save_conf=False,  # 是否在保存的标签中保存置信度
           save_crop=False,  # 是否保存裁剪的预测框
           nosave=False,  # 是否保存推理图像/视频
           classes=None,  # 过滤指定类别
           agnostic_nms=False,  # 类别无关的NMS
           augment=False,  # 增强推理
           visualize=False,  # 可视化特征
           update=False,  # 更新所有模型
           project=ROOT / 'runs/detect',  # 结果保存路径
           name='exp',  # 结果文件夹名称
           exist_ok=False,  # 是否允许已有文件夹
           line_thickness=3,  # 边框厚度
           hide_labels=False,  # 是否隐藏标签
           hide_conf=False,  # 是否隐藏置信度
           half=False,  # 是否使用FP16
           save_img=True,  # 新增：是否保存图像
           save_dir='./',  # 新增：保存图像的目录
           custom_name=None  # 新增：自定义文件名
           ):

    try:
        # 初始化
        set_logging()
        device = select_device(device)  # 选择设备（自动选择NPU/CPU）
        
        # 如果NPU可用，强制选择NPU
        if torch.npu.is_available():
            device = torch.device('npu')
        else:
            device = torch.device('cpu')

        half &= device.type != 'cpu'  # 仅在非CPU设备上启用FP16

        # 加载模型
        model = attempt_load(weights, map_location=device)  # 加载模型到指定设备
        stride = int(model.stride.max())  # 模型步长
        names = model.module.names if hasattr(model, 'module') else model.names  # 类别名称

        # 如果使用FP16推理
        if half:
            model.half()

        imgsz = check_img_size(imgsz, s=stride)  # 检查图像大小

        # 数据加载
        dataset = LoadImages(source, img_size=imgsz, stride=stride)  # 图片加载器
        bs = 1  # 批处理大小

        # 推理一次
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))

        # 推理循环
        for path, img, im0s, vid_cap in dataset:
            img = torch.from_numpy(img).to(device)  # 将输入数据移到设备
            img = img.half() if half else img.float()  # 转为FP16或FP32
            img /= 255.0  # 归一化 0 - 255 到 0.0 - 1.0
            if img.ndimension() == 3:
                img = img.unsqueeze(0)

            # 推理
            t1 = time_sync()
            pred = model(img, augment=augment)[0]

            # NMS
            pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)
            t2 = time_sync()

            # 处理预测结果
            for i, det in enumerate(pred):
                p, im0 = path, im0s
                
                # 自定义文件名逻辑
                if custom_name:
                    file_name = custom_name + f'_{i}.jpg'  # 可以加上索引避免覆盖
                else:
                    file_name = Path(p).name  # 使用原始文件名

                save_path = os.path.join(save_dir, file_name)  # 保存路径

                if len(det):
                    # 从推理图像大小缩放到原始图像大小
                    det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                    # 保存/显示结果
                    for *xyxy, conf, cls in reversed(det):
                        if save_img:  # 添加边框
                            annotator = Annotator(im0, line_width=line_thickness)
                            label = f'{names[int(cls)]} {conf:.2f}'
                            annotator.box_label(xyxy, label, color=colors(int(cls), True))

                # 显示图像
                if view_img:
                    cv2.imshow(str(p), im0)
                    cv2.waitKey(1)

                # 保存推理结果到指定目录
                if save_img:
                    cv2.imwrite(save_path, im0)

            print(f'推理完成，耗时 {t2 - t1:.3f}s')

    except SystemError as e:
        # 捕获并忽略特定的 SystemError 错误
        if "PY_SSIZE_T_CLEAN" in str(e):
            pass
        else:
            raise e  # 其他错误仍然抛出

def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default=ROOT / 'yolov5s.pt', help='模型路径')
    parser.add_argument('--source', type=str, default=ROOT / 'data/images', help='输入路径')
    parser.add_argument('--imgsz', type=int, default=640, help='图像尺寸')
    parser.add_argument('--conf-thres', type=float, default=0.25, help='置信度阈值')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='NMS IOU阈值')
    parser.add_argument('--device', default='', help='设备选择')
    parser.add_argument('--view-img', action='store_true', help='显示图像')
    parser.add_argument('--save-txt', action='store_true', help='保存到TXT')
    parser.add_argument('--half', action='store_true', help='使用FP16推理')
    parser.add_argument('--save-img', action='store_true', help='保存图像')  # 新增
    parser.add_argument('--save-dir', type=str, default='./', help='保存检测图像的目录')  # 新增
    parser.add_argument('--custom-name', type=str, help='自定义保存图像的文件名前缀')  # 新增
    return parser.parse_args()

def main(opt):
    with suppress_stderr():  # 使用 suppress_stderr 抑制错误输出
        detect(**vars(opt))

if __name__ == "__main__":
    opt = parse_opt()
    main(opt)
