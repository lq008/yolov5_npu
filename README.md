# yolov5_npu

本项目致力于实现YOLOv5 v6.0版本模型在华为昇腾NPU、鲲鹏处理器和EulerOS操作系统上的推理部署演示。

首先下载相关项目
`git clone https://gitee.com/ascend/modelzoo-GPL.git`

`cd modelzoo-GPL/built-in/PyTorch/Official/cv/object_detection/Yolov5_for_PyTorch_v6.0`

然后安装依赖环境
按照项目中的说明文档进行环境安装和配置。


最后下载本项目，运行如下命令：
`python detect_npu.py --weights <path_to_weights> --source <path_to_image> --device npu --save-img --save-dir <output_directory> --custom-name <custom_name>
`